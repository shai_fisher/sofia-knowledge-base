angular.module('sofia')
.controller('editArticleCtrl', ['$scope', 'ArticleService', 'Utils', '$location', 'Interpreter', '$q',
                              function($scope, ArticleService, Utils, $location, Interpreter, $q){
	
	var MIN_TAGS = 2;

	var articleId = $location.search().id;
	//console.log('articleId:', articleId);

	$scope.isNew = angular.isUndefined(articleId);
	//console.log('isNew:', $scope.isNew);

	$scope.editorOptions = {
	    language: 'en',
	    uiColor: '#e2dcea'
	};

	if (!$scope.isNew) {
		ArticleService.getArticle(articleId).then(function(article) {
			//console.log('article:', article);
			$scope.article = article;
			if (!article.isEditableByCurrentUser) {
				$scope.back();
			}
			$scope.originalTags = angular.copy(article.tags);
			$scope.attachments = article.attachments;
			delete $scope.article.attachments;
		})
		.catch(function(ex) {
			console.log('exception:', ex);
		});
	} else {
		$scope.article = {
			categoryId: " ",
			isDraft: false
		};
		$scope.attachments = [];
	}

	ArticleService.getAllCategories().then(function(categories) {
		$scope.allCategories = categories;
		//console.log('allCategories:', $scope.allCategories);
	});

	ArticleService.getAllTags().then(function(tags) {
		$scope.allTags = [];//tags.createMap('name');
		angular.forEach(tags, function(tag) {
			$scope.allTags.push(tag.name);
		});
		//console.log('allTags:', $scope.allTags);
	});

	/**************************** behavior ************************************/

	/*$scope.$watch('articleTagsStr', function(newValue, oldValue) {
		//console.log('articleTagsStr:', newValue);
		//str.lastIndexOf("planet");
		$scope.tagPartial = '';
		if (newValue) {
			$scope.tagPartial = newValue.substr(newValue.lastIndexOf(",")+1).trim();
			//console.log('tagPartial:', $scope.tagPartial);
		}
	});*/

	/*$scope.completeTag = function(tag) {
		$scope.articleTagsStr = Utils.replaceLast($scope.articleTagsStr, $scope.tagPartial, tag + ', ');
	};*/

	$scope.getAvailableTags = function($query) {
		return $scope.allTags.filter(function(tag) {
			return tag.toLowerCase().indexOf($query.toLowerCase()) != -1;
		});
	};

	$scope.getTagsPlaceholder = function() {
		if ($scope.saveClicked) {
			if ($scope.article && $scope.article.tags && $scope.article.tags.length > 0) {
				var length = $scope.article.tags.length;
				//console.log('length:', length, MIN_TAGS);
				if (length > 0 && length < MIN_TAGS) {
					//console.log('At least ' + (MIN_TAGS - $scope.article.tags.length) + ' more');
					return 'Add at least ' + (MIN_TAGS - $scope.article.tags.length) + ' more';
				}
			} else {
				//console.log('Add at least ' + MIN_TAGS + ' tags');
				//$scope.showTags = false;
				//$scope.showTags = true;
				return 'Add at least ' + MIN_TAGS + ' tags';
			}
		}
		return 'Add new tag';
	}

	$scope.back = function() {
		if ($scope.isNew)
			$location.url('/');
		else
			$location.url('/Article?id=' + articleId);
	};

	$scope.addFile = function(name, content) {
		//console.log('addFile:', name);
		var attachment = {
			Name: name,
			content: content
		};
		$scope.attachments.push(attachment);
		$scope.$digest();
	}

	$scope.removeFile = function(file) {
		//console.log('removeFile:', file);
		if (file.Id) {
			file.isDeleted = true;
		} else {
			scope.attachments.remove(file);
		}
	};

	/**************************** Save ************************************/

	$scope.postArticle = function() {
		//console.log('attachments:', $scope.attachments);
		//return;
		//console.log('article:', $scope.article);
		if ($scope.articleForm.$invalid || $scope.article.categoryId == ' ' || !$scope.article.tags || $scope.article.tags.length < 2) {
			//console.log('form invalid:', $scope.articleForm.$error);
			return;
		}
		var article = angular.copy($scope.article);
		delete article.postedDate;
		//article.body = Interpreter.replaceLineBreaks(article.body);
		var deletedTagsIds = organizeTags(article);
		ArticleService.postArticle(article, deletedTagsIds).then(function(articleId) {
			//console.log('articleId:', articleId);
			uploadJobs = [];
			angular.forEach($scope.attachments, function(attachment) {
				if (!attachment.Id) {
					uploadJobs.push(
						ArticleService.saveAttachment(articleId, attachment.content, attachment.Name).then(function(result){
							attachment.Id = result;
							//console.log('uploaded:', attachment.name, result);
						})
					);
				}
				else if (attachment.isDeleted) {
					uploadJobs.push(
						ArticleService.deleteAttachment(attachment.Id).then(function(){
							$scope.attachments.remove(attachment);
							//console.log('deleted:', attachment.Name);
						})
					);
				}
			});
			$q.all(uploadJobs).then(function() {
				//console.log('upload complete');
				$location.url('/Article?id=' + articleId);
			});
		});
	};

	var organizeTags = function(article) {
		//console.log('current tags:', article.tags);
		if (!article.tags)
			article.tags = [];
		var currentTagsMap = article.tags.createMap('name');
		//console.log('currentTagsMap:', currentTagsMap);

		// mark deleted tags
		var deletedTagsAssignmentsIds = [];
		angular.forEach($scope.originalTags, function(originalTag) {
			// check if deleted and re-added
			var currentTag = currentTagsMap[originalTag.name];
			if (currentTag) {
				if (!currentTag.assignmentId) {
					// restore assignmentId
					currentTag.assignmentId = originalTag.assignmentId;
				}
			} else {
				deletedTagsAssignmentsIds.push(originalTag.assignmentId);
			}
		});

		return deletedTagsAssignmentsIds;
	};

	

	$scope.deleteArticle = function() {
		//console.log('deleteArticle');
		ArticleService.deleteArticle($scope.article.id).then(function() {
			//console.log('deleted');
			$location.url('/');
		});
	}

}]);

   
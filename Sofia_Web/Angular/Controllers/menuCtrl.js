angular.module('sofia')
.controller('menuCtrl', ['$scope', 'ArticleService', 'TitlePageService', '$location','Utils',
                              function($scope, ArticleService, titlePageService, $location, Utils){
	
	//console.log('url:', $location.url());
	//console.log('path:', $location.path());
	$scope.routeParams = $location.search();
	//console.log('routeParams:', $scope.routeParams);
	var path = $location.path();
	var url = $location.url();
	if (path == '/title' || path == '/titleEdit') {
		$scope.activePage = 'title';
	}
	if (url == '/?my' || url == '/?drafts') {
		$scope.activePage = 'my';
	}

	titlePageService.getTitlePages().then(function(titlePages) {
    	//console.log('titlePages:', titlePages);
    	$scope.titlePages = titlePages;
    });

    $scope.setActivePage = function(pageName) {
    	//console.log('setActivePage:', pageName);
    	$scope.activePage = pageName;
    };

	$scope.$watchCollection(function() {
		return ArticleService.getBreadcrumbs();
	}, function(breadcrumbs) {
		$scope.category = breadcrumbs.category;
		$scope.tags = breadcrumbs.tags;
		//console.log('tags:', $scope.tags);
	});

	$scope.$watchCollection(function() {
		return titlePageService.getTitlePages();
	}, function(titlePagesPromise) {
		titlePageService.getTitlePages().then(function(titlePages) {
	    	//console.log('titlePages:', titlePages);
	    	$scope.titlePages = titlePages;
	    });
	});

	$scope.$watch('activePage', function(newValue) {
		//console.log('watch activePage:', newValue);
	});

}]);

   
angular.module('sofia')
.controller('articlesCtrl', ['$scope', 'ArticleService', '$location', 'Utils',
                              function($scope, ArticleService, $location, Utils){
	const ITEMS_IN_PAGE = 5;

	ArticleService.setBreadCrumbs($location.search());
	$scope.hasBreadcrumbs = ArticleService.hasBreadcrumbs();

	$scope.currentPage = 1;
	$scope.firstInPage = 0;
	$scope.lastInPage = ITEMS_IN_PAGE - 1;

	ArticleService.getArticles().then(function(articles) {
		//console.log('articles:', articles);
		$scope.articles = articles;
		$scope.numPages = articles.length / ITEMS_IN_PAGE;
		angular.forEach(articles, function(article) {
			article.postedDate = Utils.toJSDate(article.postedDate);
		});
	}).catch(function(ex) {
		console.log('exception:', ex);
	});

	$scope.addTagToNavigation = function(tag) {
		ArticleService.addTag(tag);
		$location.url(ArticleService.getPath());
	}

	$scope.addCategoryToNavigation = function(category) {
		ArticleService.setCategory(category);
		$location.url(ArticleService.getPath());
	}

	$scope.range = function(min, max) {
	    var range = [];
	    for (var i=min; i < max+1; i++) {
	        range.push(i);
	    }
	    return range;
	};

	$scope.navigateToPage = function(page) {
		//console.log('navigateToPage:', page);
		$scope.firstInPage = ITEMS_IN_PAGE * (page - 1);
		$scope.lastInPage = $scope.firstInPage + ITEMS_IN_PAGE - 1;
		$scope.currentPage = page;
	};
}]);

   
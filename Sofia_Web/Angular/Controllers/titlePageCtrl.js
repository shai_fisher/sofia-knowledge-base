angular.module('sofia')
.controller('titlePageCtrl', ['$scope', 'TitlePageService', '$location', 'Utils', 'Interpreter',
                              function($scope, TitlePageService, $location, Utils, Interpreter){
	
	$scope.gridsterOpts = {
		margins: [20, 20],
		outerMargin: false,
		pushing: true,
		floating: true,
		draggable: {
			enabled: false
		},
		resizable: {
			enabled: false,
			handles: ['n', 'e', 's', 'w', 'se', 'sw']
		}
	};

	var titlePageId = $location.search().id;
	//console.log('titlePageId:', titlePageId);

	TitlePageService.getTitlePage(titlePageId).then(function(titlePage) {
		//console.log('titlePage:', titlePage);
		$scope.titlePage = titlePage;
		angular.forEach(titlePage.items, function(item) {
			// fix date
			item.article.postedDate = Utils.toJSDate(item.article.postedDate);

			// fix and trust html
			item.article.bodySafe = Interpreter.trustAsHtml(item.article.body);
		});
		//console.log('items:', $scope.titlePage.items);
		$scope.isEditable = titlePage.isEditableByCurrentUser;
	});

}]);

   
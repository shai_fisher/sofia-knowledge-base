angular.module('sofia')
.controller('editArticleCtrl', ['$scope', 'ArticleService', 'Utils',
                              function($scope, ArticleService, Utils){
	
	var articleId = $location.search().id;
	console.log('postId:', postId);

	ArticleService.getArticle(articleId).then(function(article) {
		console.log('article:', article);
		$scope.article = article;

		//var bodyFixed = interpreter.fixHtmlTags(article.body);
		//console.log('bodyFixed:', bodyFixed);
		//article.bodySafe = $sce.trustAsHtml(bodyFixed);

		article.postedDate = Utils.toJSDate(article.postedDate);

	}).catch(function(ex) {
		console.log('exception:', ex);
	});

	$scope.postArticle = function() {

		$scope.article.tags = Utils.split($scope.article.tags);
		//console.log('tags:', $scope.article.tags);
		console.log('article:', $scope.article);
		ArticleService.postArticle($scope.article).then(function(articleId) {
			console.log('articleId:', articleId);
			//$location.url('/post?id=' + postId);
		});
	}

}]);

   
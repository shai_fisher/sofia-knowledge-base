angular.module('sofia')
.controller('articleCtrl', ['$scope', 'ArticleService', '$location', '$sce', 'Interpreter', 'Utils', '$timeout',
                              function($scope, ArticleService, $location, $sce, Interpreter, Utils, $timeout){
	
	$scope.data = {
		editComment: null
	};

	var articleId = $location.search().id;

	//console.log('articleId:', articleId);

	ArticleService.getArticle(articleId).then(function(article) {
		console.log('article:', article);
		$scope.article = article;
		$scope.isEditable = article.isEditableByCurrentUser;

		//console.log('body:', article.body);
		article.bodySafe = Interpreter.trustAsHtml(article.body);
		//console.log('bodySafe:', article.bodySafe);

		article.postedDate = Utils.toJSDate(article.postedDate);

		angular.forEach(article.comments, function(comment) {
			comment.postedDate = Utils.toJSDate(comment.postedDate);
		});

		angular.element(document).ready(function () {
			$timeout(function() {
				//console.log('highlighting');
				SyntaxHighlighter.config.stripBrs = true;
				SyntaxHighlighter.highlight();
			}, 500);
	        
	    });
		
	}).catch(function(ex) {
		console.log('exception:', ex);
	});


	$scope.postComment = function() {
		$scope.newComment.articleId = $scope.article.id;
		ArticleService.postComment($scope.newComment).then(function(comment) {
			//console.log('comment:', comment);
			comment.postedDate = Utils.toJSDate(comment.postedDate);
			comment.isEditableByCurrentUser = true;
			$scope.article.comments.push(comment);
			$scope.newComment = {};
		});
	};

	$scope.updateComment = function(comment) {
		commentCopy = angular.copy(comment);
		delete commentCopy.postedDate;
		//console.log('updating comment:', commentCopy);
		ArticleService.postComment(commentCopy).then(function(updatedComment) {
			//console.log('updatedComment:', updatedComment);
			$scope.data.editComment = null;
		});
	};

	$scope.deleteComment = function(comment, index) {
		//console.log('deleteComment:', comment);
		ArticleService.deleteComment(comment.id).then(function() {
			$scope.article.comments.splice(index, 1);
		});
	};

	$scope.toggleLikeArticle = function() {
		//console.log('toggleLikeArticle');
		if ($scope.article.isLikedByCurrentUser) {
			ArticleService.dislikeArticle($scope.article.id).then(function() {
				//console.log('toggleLikeArticle: disliked');
				$scope.article.isLikedByCurrentUser = false;
				$scope.article.likes--;
			});
		} else {
			ArticleService.likeArticle($scope.article.id).then(function() {
				//console.log('toggleLikeArticle: liked');
				$scope.article.isLikedByCurrentUser = true;
				$scope.article.likes++;
			});
		}
	};

}]);

   
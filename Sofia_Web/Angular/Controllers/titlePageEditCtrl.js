angular.module('sofia')
.controller('titlePageEditCtrl', ['$scope', 'TitlePageService', 'ArticleService', '$location', 'Utils',
							function($scope, TitlePageService, ArticleService, $location, Utils){
	
	$scope.gridsterOpts = {
		margins: [20, 20],
		outerMargin: false,
		pushing: true,
		floating: true,
		draggable: {
			enabled: true
		},
		resizable: {
			enabled: true,
			handles: ['n', 'e', 's', 'w', 'se', 'sw']
		}
	};

	var loadAvailableArticles = function() {
		ArticleService.resetBreadCrumbs();
		ArticleService.getArticles().then(function(articles) {
			$scope.articles = articles;

			var itemsMap = $scope.titlePage.items.createMap('articleId');
			//console.log('itemsMap:', itemsMap);
			angular.forEach(articles, function(article) {
				article.postedDate = Utils.toJSDate(article.postedDate);
				article.isIncluded = (itemsMap[article.id]) ? true : false;
				if (article.isIncluded) {
					itemsMap[article.id].article = article;
				}
			});
			//console.log('articles:', articles);
		}).catch(function(ex) {
			console.log('exception:', ex);
		});
	};

	var titlePageId = $location.search().id;
	//console.log('titlePageId:', titlePageId, $location.search());

	if (titlePageId) {
		//console.log('titlePageId:', titlePageId);
		TitlePageService.getTitlePage(titlePageId).then(function(titlePage) {
			//console.log('titlePage:', titlePage);
			$scope.titlePage = titlePage;
			titlePage.deletedItems = [];
			//console.log('items:', $scope.titlePage.items);
			loadAvailableArticles();
		});
	}
	else {
		$scope.titlePage = {
			name: 'New Title Page',
			items: []
		};
		$scope.editName = true;
		$scope.isNew = true;
		loadAvailableArticles();
	}

	
	

	$scope.addItem = function(article) {
		$scope.titlePage.items.push( {
			titlePageId: $scope.titlePage.id,
			articleId: article.id,
			article: article,
			sizeX: 2,
			sizeY: 1
		});
		article.isIncluded = true;
	};

	$scope.removeItem = function(index, item) {
		//console.log('removeItem:', index, item);
		item.article.isIncluded = false;
		$scope.titlePage.deletedItems.push(item);
		$scope.titlePage.items.splice(index, 1);
	};


	$scope.back = function() {
		console.log('path:', '/title?id=' + titlePageId);
		$location.url('/title?id=' + titlePageId);
	};

	$scope.save = function() {
		var titlePageCopy = angular.copy($scope.titlePage);
		angular.forEach(titlePageCopy.items, function(item) {
			delete item.article;
		});
		angular.forEach(titlePageCopy.deletedItems, function(item) {
			delete item.article;
		});
		//console.log('save titlePageCopy:', titlePageCopy);
		TitlePageService.saveTitlePage(titlePageCopy).then(function(titlePageId) {
			//console.log('saveTitlePage:', titlePageId);
			TitlePageService.refreshTitlePages();
			$location.url('/title?id=' + titlePageId);
		}).catch(function(ex) {
			console.log(ex);
		});
	};

	$scope.deleteTitlePage = function() {
		TitlePageService.deleteTitlePage(titlePageId).then(function() {
			$location.url('/');
		});
	}

}]);

   
angular.module('sofia')
.directive('uploadArea', ['$q','$document','ArticleService',
                          function($q, $document, ArticleService){  
	return {
	    restrict: 'ECA',
	    template: '<input style="display:none;"  id="fileInput" type="file" accept="image/*" capture="camera"></input>' + 
	    			'<div class="dropZone" ng-class="{\'dropZone-error\':hasError}">' +
	    				'<div class="dropZone" id="drop_zone" style="opacity: 0; position: absolute; margin: 0; z-index: 1;">' +
	    					'<div id="dropzone_preview" class="dropzone-previews" style="display:none"></div>' +
	    				'</div> <span class="lbl" ng-bind="displayLabel"/></div>' + 
	    			'<div class="wrap-upload-ico"><uploading-loader display-loader="{{uploadStarted}}"></uploading-loader></div>',
		scope: {
        	label: '@',
        	wrongSizeError: '@',
        	onFileAdd: '&'
    	},
	    replace: false,
	    link:function (scope, element, attrs){
	    	
	    	//var imageFileTypes = ["image/png","image/x-icon","image/jpeg","image/bmp"];
	    	if (!scope.filesContainer) {
	    		scope.filesContainer = [];
	    	}
	    	
	    	// catch file and invoke saveNewFile
	    	var catchFile = function (file) {
	    		//console.log('catchFile: ', file);
				scope.clearError();
                
                if (!file)
                	return;
                
                scope.uploadStarted = 'true';
                //console.log('uploadStarted: true');
                scope.$digest();

				// check size
				if (file.size > 740793) {
					console.log('illegal file size: ', file.size);
					handleError(scope.wrongSizeError);
					return;
				}
				
				var filename = (scope.fileName ? scope.fileName : file.name);
				var veritableFileName = file.name;
				var reader = new FileReader();
				reader.onload = function(e) {
					var imgData = e.target.result;
					imgData = imgData.split('base64,').pop();

					var fileAddFunction = scope.onFileAdd();
					fileAddFunction(file.name, imgData);
				};
				reader.readAsDataURL(file);
			};
			
			// set dropzone
	    	var dropzoneConfig = {
	    			  //parallelUploads: 1,
	    			  //maxFileSize: 30,
	    			  url: "/",
	    			  uploadMultiple: false,
	    			  createImageThumbnails: false,
	    			  autoProcessQueue: false,
	    			  previewsContainer:"#dropzone_preview"
	    			};
	    	//console.log('element: ', element[0].children[1]);
	    	var dropzone = new Dropzone(element[0].children[1].children[0], dropzoneConfig);
	    	// bind the given event handlers
	    	dropzone.on('addedfile', catchFile);
	    	
	    	scope.displayLabel = scope.label;
	    	
	    	// display arc loader for testing
	    	//scope.uploadStarted = true;
	    	
	    	var handleError = function(error) {
	    		console.log('handleError:', error);
	    		scope.hasError = true;
	    		scope.displayLabel = error;
	    		scope.uploadStarted = false;
	    		scope.$apply();
	    	};
	    	
	    	scope.clearError = function() {
	    		//console.log('clearError');
	    		scope.hasError = false;
	    		scope.displayLabel = scope.label;
	    	};
	    	
	    }   
	};
}]);
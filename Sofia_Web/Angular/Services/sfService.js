angular.module('sofia')
.factory('sfService', ['settings','$q', function (settings,$q) {
	
	// var utilsData = {
 //        currentZone : moment().zone()
 //    }
	
	return {
		getPicklistVal: function(ctrlName, functionName, objName, fieldNameList, addEmpty) {
            var addNamespace = function(){
                if(objName.indexOf('__c') > 0)
                   objName = settings.NSField + objName;
            }

            addNamespace();
            var deferred = $q.defer();
            
            Visualforce.remoting.Manager.invokeAction(
                settings.NSClass + ctrlName + '.' + functionName, objName, fieldNameList, !!addEmpty, function(result, event) {
                    if (event.status) {
                        deferred.resolve(result);
                    } else {
                        deferred.reject(event);
                    }   
            },{ buffer: true, escape: true, timeout: 30000 });
                
            return deferred.promise;
        },

        analyseResult: function(result){
			//console.log('analyseResult: ' , result);
            var regex = new RegExp(settings.NSField,"g");
            return angular.fromJson(result.replace(regex, ''));
        },
        
        prepareBeforeSave: function(objectList, isArrayOld){
			var isArray = angular.isArray(objectList);
            var clonedObjectList = angular.copy(objectList);
			if (!isArray)
				clonedObjectList = [clonedObjectList];	// wrap in array
            for(var index = 0; index < clonedObjectList.length; index++ ){
                for(var key in clonedObjectList[index]){
                    if(key.indexOf("__r") > -1)
                        delete clonedObjectList[index][key];
                    if(key.indexOf("__c") > -1 && settings.NSField != ''){
                        clonedObjectList[index][settings.NSField + key] = clonedObjectList[index][key];
                        delete clonedObjectList[index][key];
                    }
                }
            }
			//console.log('after prepare: ', clonedObjectList);
            if(!isArray && clonedObjectList.length > 0)
                return angular.toJson(clonedObjectList[0]);
             return angular.toJson(clonedObjectList);
        },
		
		// Convert date to SF format
		// needs import of: moment.js
		dateToSfFormat: function(date){
            if(date instanceof Date)
                date = moment(date).add(utilsData.currentZone * -1, 'm').valueOf();
            return date;
        },
			
		
		mapSize: function(mapObject) {
			var size = 0, key;
			for (key in mapObject) {
				if (mapObject.hasOwnProperty(key)) size++;
			}
			return size;
		},
		
		callRemoteSF: function(fncName, toAnalyze /*[arguments,...,]*/){
			var self = this;
			var args = Array.prototype.slice.call(arguments);
			var deferred = $q.defer();  

			for (var i = 2; i < args.length; i++) {
				if (angular.isArray(args[i])) {
					if (args[i].length > 0 && angular.isObject(args[i][0]))
						args[i] = self.prepareBeforeSave(args[i], true);
				}
				else if (angular.isObject(args[i])) {
					args[i] = self.prepareBeforeSave(args[i], false);
				}
				
			}
			var toAna = args.splice(1, 1)[0];
			
			args.push(function(result, event){
				if (event.status) {
					if(toAna)
						deferred.resolve(self.analyseResult(result));
					else
						deferred.resolve(result);
				}
				else 						  
					deferred.reject(event);
			});
			
			args[0] = settings.NSClass + args[0];
			args.push({ buffer: true, escape: false, timeout: 30000 });
			
			Visualforce.remoting.Manager['invokeAction'].apply(Visualforce.remoting.Manager, args);
			return  deferred.promise;
		}
	};
}])

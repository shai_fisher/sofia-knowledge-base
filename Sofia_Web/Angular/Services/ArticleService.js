angular.module('sofia')
.factory('ArticleService', ['sfService', function( sfService) {
	var promises = {
		users: null,
        getArticles: null
	};

    var breadcrumbs = {
        category: '',
        tags: [],
        search: '',
        myArticles: false,
        drafts: false
    };

	return {

        //---------------------- Breadcrumbs (path) functions -------------------
        getBreadcrumbs: function() {
            return breadcrumbs;
        },

        hasBreadcrumbs: function() {
            return breadcrumbs.category || breadcrumbs.tags.length > 0 || breadcrumbs.search;
        },

        /*getCategory: function(category) {
            return breadcrumbs.category;
            //console.log('breadcrumbs:', breadcrumbs);
        },*/

        setCategory: function(category) {
            breadcrumbs.category = category || '';
        },

        setTags: function(tagsStr) {
            //console.log('setTags:', tagsStr);
            breadcrumbs.tags = (tagsStr) ? tagsStr.split(',') : [];
            //console.log('breadcrumbs:', breadcrumbs);
        },

        setBreadCrumbs: function(pageParams) {
            //console.log('setBreadCrumbs:', pageParams);
            breadcrumbs.category = pageParams.category;
            var tagsStr = pageParams.tags || pageParams.tag;
            breadcrumbs.tags = (tagsStr) ? tagsStr.split(',') : [];
            breadcrumbs.myArticles = pageParams.my || pageParams.drafts || false;
            breadcrumbs.drafts = pageParams.drafts || false;
            breadcrumbs.search = pageParams.search;

            promises.getArticles = null;
        },

        resetBreadCrumbs: function() {
            this.setBreadCrumbs({});
        },

        addTag: function(tag) {
            if (breadcrumbs.tags.indexOf(tag) == -1) {
                //console.log('addTag:', tag);
                breadcrumbs.tags.push(tag);
            }
        },

        getPath: function() {
            //console.log('getPath:', breadcrumbs);
            var params = [];
            if (breadcrumbs.category) {
                params.push('category=' + breadcrumbs.category);
            }
            if (breadcrumbs.tags && breadcrumbs.tags.length > 0) {
                params.push('tags=' + breadcrumbs.tags.join());
            }
            if (params.length > 0) {
                return '/?' + params.join('&');
            } else {
                return '/';
            }
        },

        //---------------------- end of Breadcrumbs (path) functions -------------------

    	getArticles: function() {
            if (!promises.getArticles) {
			    promises.getArticles = sfService.callRemoteSF('Ctrl_Main.getArticles', true, breadcrumbs);
            }
            return promises.getArticles;
    	},

    	getArticle: function(articleId) {
			return sfService.callRemoteSF('Ctrl_Main.getArticle', true, articleId);
    	},

    	postComment: function(comment) {
    		return sfService.callRemoteSF('Ctrl_Main.postComment', true, comment);
    	},

        deleteComment: function(commentId) {
            return sfService.callRemoteSF('Ctrl_Main.deleteComment', false, commentId);
        },

        likeArticle: function(articleId) {
            return sfService.callRemoteSF('Ctrl_Main.likeArticle', false, articleId);
        },

        dislikeArticle: function(articleId) {
            return sfService.callRemoteSF('Ctrl_Main.dislikeArticle', false, articleId);
        },

    	postArticle: function(article, deletedTagsIds) {
    		return sfService.callRemoteSF('Ctrl_Main.postArticle', false, article, deletedTagsIds);
    	},

        getAllTags: function() {
            return sfService.callRemoteSF('Ctrl_Main.getAllTags', true);
        },

        getAllCategories: function() {
            return sfService.callRemoteSF('Ctrl_Main.getAllCatgories', true);
        },

        deleteArticle: function(articleId) {
            return sfService.callRemoteSF('Ctrl_Main.deleteArticle', false, articleId);
        },

        saveAttachment: function(parentId,base64str,fileName) {
            //console.log('parentId: ', parentId, ', fileName: ', fileName, ', size: ', base64str.length);
            return sfService.callRemoteSF('Ctrl_Main.saveAttachment', false, parentId, base64str, fileName);
        },

        deleteAttachment: function(attachmentId) {
            return sfService.callRemoteSF('Ctrl_Main.deleteAttachment', false, attachmentId);
        },

    };

}]);;
angular.module('sofia')
.factory('TitlePageService', ['sfService', '$q', function(sfService, $q) {
	var promises = {
        titlePages: null
	};

    var breadcrumbs = {
        category: '',
        tags: [],
        search: '',
        myArticles: false
    };

	return {

        getTitlePage: function(titlePageId) {
            return sfService.callRemoteSF('Ctrl_Main.getTitlePage', true, titlePageId);
        },

        getTitlePages: function() {
            if (!promises.titlePages) {
                promises.titlePages = sfService.callRemoteSF('Ctrl_Main.getTitlePages', true);
            }
            return promises.titlePages;
        },

        refreshTitlePages: function() {
            promises.titlePages = null;
        },

        saveTitlePage: function(titlePage) {
            return sfService.callRemoteSF('Ctrl_Main.saveTitlePage', false, titlePage);
        },

        deleteTitlePage: function(titlePageId) {
            // delete from list (to update list in menu bar)
            if (promises.titlePages) {
                promises.titlePages.then(function(titlePages) {
                    var index = titlePages.getIndexByValue('Id', titlePageId);
                    if (index > -1) {
                        titlePages.splice(index, 1);
                    }
                });
            }
            return sfService.callRemoteSF('Ctrl_Main.deleteTitlePage', false, titlePageId);
        },
    	
    };

}]);
angular.module('sofia')
.factory('Interpreter', ['sfService', '$sce', function(sfService, $sce) {
	
	return {
        replaceAll: function(text, find, replace) {
            if (!text)
                return text;
            //str = str.replace(/abc/g, '');
            var regEx = new RegExp(find, 'g');
            return text.replace(regEx, replace);
        },

		fixHtmlTags: function(text) {
            text = this.replaceAll(text, '&lt;', '<');
            text = this.replaceAll(text, '&gt;', '>');
            text = this.replaceAll(text, '&quot;', '"');
            text = this.replaceAll(text, '<br>', '<br>\n');
			return text;
    	},

        replaceLineBreaks: function(text) {
            return this.replaceAll(text, '\n', '<BR/>');
        },

        replaceBRs: function(text) {
            text = this.replaceAll(text, '<BR/>', '\n');
            text = this.replaceAll(text, '<BR>', '\n');
            text = this.replaceAll(text, '<br>', '\n');
            text = this.replaceAll(text, '<br/>', '\n');
            return text;
        },

        trustAsHtml: function(body) {
            var bodyFixed = this.fixHtmlTags(body);
            return $sce.trustAsHtml(bodyFixed);
        },


    };

}]);;
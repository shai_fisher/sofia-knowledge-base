angular.module('sofia')
.factory('Utils', [function() {
	
	var utils = {
        replaceAll: function(text, find, replace) {
            //str = str.replace(/abc/g, '');
            var regEx = new RegExp(find, 'g');
            return text.replace(regEx, replace);
        },

        /*
         * Converts SF date to JS date. i.e.: 2015-12-28T18:08:45.000Z => 2015/12/28 18:08
         */
		toJSDate: function(sfDateStr) {
            if (!sfDateStr)
                return '';
            return new Date(sfDateStr)
        },

        /*
         * Converts SF date to JS date. i.e.: 2015-12-28T18:08:45.000Z => 2015/12/28 18:08
         */
        toDateStr: function(sfDate) {
            if (!sfDate)
                return '';
            var sfdateSplit = sfDate.split('T');
            var sftime = sfdateSplit[1].split(':');
            sfdateSplit[0] = this.replaceAll(sfdateSplit[0], '-', '/');
            return sfdateSplit[0] + ' ' + sftime[0] + ':' + sftime[1];
        },

        containsString: function(text, subtext) {
            if (!text)
                return false;
            return (text.indexOf(subtext) > -1);
        },

        replaceLast: function(str, substr, replacement) {
            return str.substr(0, str.lastIndexOf(substr)) + replacement;
        },

        split: function(str) {
            if (!str)
                return [];

            if (typeof str != 'string')
                return str;
            
            str = this.replaceAll(str, ' ', ',');
            str = this.replaceAll(str, ';', ',');
            while (str.indexOf(',,') > -1) {
                str = this.replaceAll(str, ',,', ',');
            }
            // remove last comma
            if (str.slice(-1) ==',') {
                str = str.substring(0, str.length - 1);
            }
            return str.split(",");
        },

        /*getValues: function(array, field) {
            var values = [];
            angular.forEach(array, function(object) {
                values.push(object[field]);
            });
            return values;
        },

        createMap: function(array, keyField) {
            var map = {};
            angular.forEach(array, function(item) {
                var key = item[keyField];
                map[key] = item;
            });
            return map;
        },*/

        toArray: function(map) {
            var array = [];
            angular.forEach(map, function(item, key) {
                array.push(item);
            });
            return array;
        }

    };

    // attach some functions to prototypes
    Array.prototype.createMap = function(keyField) {
        var map = {};
        angular.forEach(this, function(item) {
            var key = item[keyField];
            map[key] = item;
        });
        return map;
    };

    Array.prototype.getValues = function(keyField) {
        var values = [];
        angular.forEach(this, function(object) {
            values.push(object[keyField]);
        });
        return values;
    };

    Array.prototype.getIndexByValue = function(keyField, keyValue) {
        for (var i=0; i < this.length; i++) {
            var item = this[i];
            if (item[keyField] == keyValue) {
                return i;
            }
        }
        return -1;
    };


    return utils;

}]);;
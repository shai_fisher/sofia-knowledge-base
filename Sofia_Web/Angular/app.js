angular.module('sofia', ['ngRoute', 'settings', 'ngCkeditor', 'ngTagsInput', 'gridster'])
.config(['$routeProvider', function($routeProvider) {
  $routeProvider
	.when('/', {
      templateUrl: 'Main_ArticlesList',
      controller: 'articlesCtrl'
  })
  .when('/Article', {
      templateUrl: 'Main_Article',
      controller: 'articleCtrl'
  })
  .when('/New', {
      templateUrl: 'Main_EditArticle',
      controller: 'editArticleCtrl'
  })
  .when('/Edit', {
      templateUrl: 'Main_EditArticle',
      controller: 'editArticleCtrl'
  })
  .when('/title', {
      templateUrl: 'Main_TitlePage',
      controller: 'titlePageCtrl'
  })
  .when('/titleEdit', {
      templateUrl: 'Main_TitlePageEdit',
      controller: 'titlePageEditCtrl'
  })
  .otherwise({
   	redirectTo: '/'  
  });
}]);



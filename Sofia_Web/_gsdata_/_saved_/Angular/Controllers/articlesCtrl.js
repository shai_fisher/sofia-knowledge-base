angular.module('sofia')
.controller('articlesCtrl', ['$scope', 'ArticleService', '$location', 'Utils',
                              function($scope, ArticleService, $location, Utils){
	
	var tags = $location.search().tag;
	console.log('tags:', tags);

	ArticleService.getAllArticles().then(function(posts) {
		console.log('posts:', posts);
		$scope.posts = posts;
	}).catch(function(ex) {
		console.log('exception:', ex);
	});

	$scope.changeTag = function(tag) {
		//$location.url('/Track_History?id=' + trackId);
		ArticleService.getArticlesByTags([tag]).then(function(posts) {
			console.log('posts:', posts);
			$scope.posts = posts;
		}).catch(function(ex) {
			console.log('exception:', ex);
		});
	};

	$scope.goto = function(destination) {
		$location.url('/New');
	};
}]);

   
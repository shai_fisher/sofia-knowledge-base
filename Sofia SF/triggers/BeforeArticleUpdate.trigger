trigger BeforeArticleUpdate on Article__c (before insert, before update) {

	for (Article__c article : trigger.New) {
		// count likes
		article.Likes_Num__c = 0;
		if (String.isNotEmpty(article.Likes__c)) {
			article.Likes_Num__c = article.Likes__c.split(';').size();
		}
	}

}
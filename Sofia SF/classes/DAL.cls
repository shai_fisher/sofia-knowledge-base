public with sharing class DAL {
	
	private static final Integer MAX_RECORDS = 1000;
	private static final String INTERNAL_ZONE_NAME = 'Internal Zone';


	/****************************************** Articles *************************************/

	/*public static List<Article__c> getAllArticles() {
		return [select Id, Title__c, Short_Description__c, Category__r.Name, CreatedBy.Name, CreatedDate,
				OwnerId,
				(select TagDefinitionId, Name from Tags)
		 		from Article__c limit :MAX_RECORDS];
	}*/

	/*
	 * Return articles by tag names
	 * Costs 2 queries: one for Article__Tag junction object, and one for Article__c
	 *
	 * (select articles directly according to the Article__Tag junction is impossible
	 *   produces an error: "Save error: Entity 'Article__Tag' is not supported for semi join inner selects"
	 * select junction with parents fields also impossible
	 *   cannot select childs tags of article parent)
	 */
	public static List<Article__c> getArticles(Boolean myArticles, Boolean drafts, String category, Set<String> tagsNames, String search) {
		Utils.log('getArticles: ' + myArticles + ', category:' + category + ', tagsNames:' + tagsNames);

		String queryStr = 'select Id, Title__c, Short_Description__c, Category__r.Name, CreatedBy.Name, ' + 
			'CreatedDate, Published_Date__c, OwnerId, IsDraft__c, Likes_Num__c, ' +
			'(select TagDefinitionId, Name, ItemId from Tags) ' + 
	 		'from Article__c';

	 	List<String> conditions = new List<String>();

	 	if (myArticles == true) {	// may also be null and throw exception
	 		conditions.add('OwnerId = \'' + UserInfo.getUserId() + '\'');
	 		if (drafts == true) {
	 			conditions.add('IsDraft__c = true');
	 		}
	 	} else {
	 		conditions.add('IsDraft__c = false');
	 	}
	 	if (String.isNotEmpty(category)) {
	 		conditions.add('Category__r.Name = \'' + category + '\'');
	 	}
		if (tagsNames != null && !tagsNames.isEmpty()) {
			// collect articles ids
			Set<Id> articlesIds = getArticlesIds(tagsNames);
			Utils.log('articlesIds: ' + articlesIds);
			conditions.add('Id in :articlesIds');
		}
		if (String.isNotEmpty(search)) {
			List<List<SObject>> result = [FIND :search IN ALL FIELDS RETURNING sofia__Article__c];
			List<Article__c> articles = (List<Article__c>) result.get(0);
			conditions.add('Id in :articles');
		}
		if (conditions.size() > 0) {
			queryStr += ' where ' + String.join(conditions, ' and ');
		}

		queryStr += ' order by CreatedDate limit :MAX_RECORDS';
		Utils.log('queryStr: ' + queryStr);
		return Database.query(queryStr);
	}

	

	public static List<Article__c> searchArticles(String query) {
		List<List<SObject>> result = [FIND :query IN ALL FIELDS RETURNING sofia__Article__c];
		List<Article__c> articles = (List<Article__c>) result.get(0);
		return [select Id, Title__c, Short_Description__c, Category__r.Name, CreatedBy.Name, CreatedDate, Published_Date__c,
				(select TagDefinitionId, Name, ItemId from Tags)
		 		from Article__c where Id in :articles order by CreatedDate limit :MAX_RECORDS];
		return articles;
	}

	private static Set<Id> getArticlesIds(Set<String> tagsNames) {
		// get Article-Tag junction records
		List<Article__Tag> tagAssignments = [select ItemId from Article__Tag where Name in :tagsNames];
		
		// collect articles ids
		Map<Id,Integer> articlesCountersMap = new Map<Id,Integer>();
		for (Article__Tag tagAssignment : tagAssignments) {
			Integer count = articlesCountersMap.get(tagAssignment.ItemId);
			if (count == null)
				count = 0;
			articlesCountersMap.put(tagAssignment.ItemId, count + 1);
		}

		// check which articles tagged with all tags
		Set<Id> articlesIds = new Set<Id>();
		for (Id articlesId : articlesCountersMap.keySet()) {
			if (articlesCountersMap.get(articlesId) == tagsNames.size()) {
				articlesIds.add(articlesId);
			}
		}
		return articlesIds;
	}

	public static Article__c getArticle(String articleId) {
		List<Article__c> articles = [select Id, Title__c, Body__c, Short_Description__c, Category__r.Name, 
				CreatedBy.Name, CreatedBy.Contact.Name, CreatedDate, Published_Date__c, OwnerId, Likes__c, IsDraft__c, Likes_Num__c,
				(select Id, Body__c, CreatorName__c, CreatedDate, CreatedBy.Name from Comments__r order by CreatedDate),
				(select TagDefinitionId, Name, ItemId from Tags),
				(select Id, Name from Attachments)
				from Article__c where Id = :articleId limit 1];
		if (!articles.isEmpty())
			return articles[0];
		return null;
	}

	public static Article__c getArticleLikes(String articleId) {
		List<Article__c> articles = [select Id, Likes__c
				from Article__c where Id = :articleId limit 1];
		if (!articles.isEmpty())
			return articles[0];
		return null;
	}

	/****************************************** End of Articles *************************************/


	/****************************************** Comments *************************************/

	public static Comment__c getComment(String commentId) {
		List<Comment__c> comments = [select Id, CreatorName__c, Body__c, CreatedDate
				from Comment__c where Id = :commentId limit 1];
		if (!comments.isEmpty())
			return comments[0];
		return null;
	}

	/****************************************** End of Comments *************************************/




	/****************************************** Categories *************************************/

	public static List<Category__c> getAllCategories() {
		return [select Id, Name from Category__c limit :MAX_RECORDS];
	}

	/****************************************** End of Categories *************************************/



	/****************************************** Ideas *************************************/

	/*public static List<Idea> getAllIdeas() {
		return [select Id, Title, Short_Description__c, Category__r.Name, Tags__c, CreatorName, CreatedDate from Idea limit :MAX_RECORDS];
	}

	public static List<Idea> getIdeasByTags(List<String> tags) {
		if (tags == null || tags.isEmpty())
			return getAllIdeas();

		String queryStr = 'select Id, Title, Short_Description__c, Category__r.Name, Tags__c, CreatorName, CreatedDate ' +
					' from Idea where ' + 
					'Tags__c like \'%' + String.join(tags, '%\' and Tags__c like \'%') + '%\'' + 
					'limit :MAX_RECORDS';
		Utils.log('getIdeasByTags: queryStr: ' + queryStr);

		return Database.query(queryStr);
	}

	public static Idea getIdea(String ideaId) {
		List<Idea> ideas = [select Id, Title, Body, Short_Description__c, Category__r.Name, Tags__c,
				CreatedBy.Contact.Name, CreatorName, CreatedDate, 
				(Select Id, CreatorName, CommentBody, CreatedDate From Comments)
				from Idea where Id = :ideaId limit 1];
		if (!ideas.isEmpty())
			return ideas[0];
		return null;
	}
	
	public static String getInternalZoneCommunityId() {
		List<Community> communities = [select Id from Community where Name = :INTERNAL_ZONE_NAME];
		if (!communities.isEmpty()) {
			return communities[0].Id;
		}
		return null;
	}

	public static IdeaComment getIdeaComment(String ideaCommentId) {
		List<IdeaComment> ideaComments = [select Id, CreatorName, CommentBody, CreatedDate
				from IdeaComment where Id = :ideaCommentId limit 1];
		if (!ideaComments.isEmpty())
			return ideaComments[0];
		return null;
	}*/

	/****************************************** End of Ideas *************************************/

	/****************************************** Tags *************************************/

	public static List<TagDefinition> getAllTags() {
		return [select Id, Name from TagDefinition];
	}

	/****************************************** end of Tags *************************************/

	/****************************************** Title Pages *************************************/

	public static List<Title_Page__c> getTitlePages() {
		return [select Id, Name from Title_Page__c];
	}
	
	public static Title_Page__c getTitlePage(String titlePageId) {
		Utils.log('getTitlePage: ' + titlePageId);
		List<Title_Page__c> titlePages = [select Id, Name, CreatedById, OwnerId,
			(select Id, Article__c, Row__c, Col__c, Rowspan__c, Colspan__c,
				Article__r.Title__c, Article__r.Short_Description__c, Article__r.CreatedDate, Article__r.Published_Date__c, Article__r.Category__r.Name,
				Article__r.CreatedBy.Name, Article__r.OwnerId, Article__r.IsDraft__c, Article__r.Body__c, Article__r.Likes__c, Article__r.Likes_Num__c
				from Title_Page_Items__r order by Row__c, Col__c)
			 from Title_Page__c where Id = :titlePageId];
		if (!titlePages.isEmpty()) {
			return titlePages[0];
		}
		return null;
	}
}
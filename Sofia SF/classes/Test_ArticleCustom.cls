@isTest
private class Test_ArticleCustom {
	
	private static testMethod void test_ArticleConstructor() {
		Article__c articleSF = Test_Factory.createArticleSF();
		ArticleCustom article = new ArticleCustom(articleSF);

		System.assertEquals(articleSF.Id, article.id);
	}
	
	private static testMethod void test_fromSobjects() {
		Article__c articleSF = Test_Factory.createArticleSF();
		List<Article__c> articlesSF = new List<Article__c> { articleSF };

		List<ArticleCustom> articles = ArticleCustom.fromSobjects(articlesSF);
	}

	/*private static testMethod void test_getAllArticles() {
		Article__c articleSF = Test_Factory.createArticleSF();

		List<ArticleCustom> articles = ArticleCustom.getAllArticles();
		System.assertEquals(1, articles.size());
	}*/

	private static testMethod void test_getArticles() {
		Category__c category = Test_Factory.createCategory();
		Article__c articleSF = Test_Factory.createArticleSF(category);
		Test_Factory.addTag(articleSF, 'tag1');

		Set<String> tags = new Set<String> { 'tag1' };
		//List<ArticleCustom> articles = ArticleCustom.getArticles(category.Name, tags);
		//System.assertEquals(1, articles.size());
	}
	
}
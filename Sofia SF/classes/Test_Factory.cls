public with sharing class Test_Factory {

	public static Category__c createCategory() {
		Category__c category = new Category__c(Name = 'test');
		insert category;
		return category;
	}

	public static Article__c createArticleSF() {
		Category__c category = createCategory();
		return createArticleSF(category);
	}

	public static Article__c createArticleSF(Category__c category) {
		Article__c article = new Article__c(
			Title__c = 'Title',
			Short_Description__c = 'Short',
			Category__c = category.Id,
			IsDraft__c = false
		);
		insert article;
		addTag(article);
		return article;
	}

	public static ArticleCustom createArticle() {
		Article__c articleSF = createArticleSF();
		return ArticleCustom.getArticle(articleSF.Id);
	}

	public static void addTag(Article__c article) {
		addTag(article, 'tag1');
	}

	public static void addTag(Article__c article, String tag) {
		Article__Tag tagAssignment = new Article__Tag(
			Name = tag,
			ItemId = article.Id,
			Type = 'Public'
		);
		insert tagAssignment;
	}

	public static Attachment createAttachment(String parentId) {
		Attachment attachment = new Attachment(
			Name = 'test',
			ParentId = parentId,
			Body = Blob.valueOf('test64')
		);
		insert attachment;
		return attachment;
	}

	public static Title_Page__c createTitlePage(String name) {
		Title_Page__c titlePage = new Title_Page__c(Name = name);
		insert titlePage;
		return titlePage;
	}

	public static Title_Page_Item__c createTitlePageItem(Title_Page__c titlePage) {
		Article__c articleSF = createArticleSF();
		return createTitlePageItem(titlePage, articleSF);
	}

	public static Title_Page_Item__c createTitlePageItem(Title_Page__c titlePage, Article__c article) {
		Title_Page_Item__c titlePageItem = new Title_Page_Item__c(
			Title_Page__c = titlePage.Id,
			Article__c = article.Id
		);
		insert titlePageItem;
		return titlePageItem; 
	}
}
public with sharing class TagCustom {

	private static final String TAG_TYPE_PUBLIC = 'Public';

	public String name 				{get; private set;}			// tag name
	private String assignmentId;	// Article__Tag assignment id


	
	public TagCustom(Article__Tag tagAssignment) {
		name = tagAssignment.Name;
		assignmentId = tagAssignment.Id;
	}

	public TagCustom(TagDefinition tagSF) {
		name = tagSF.Name;
	}

	public Article__Tag toSObject() {
		Article__Tag tagSF = new Article__Tag(
			Id = assignmentId,
			Name = name,
			Type = TAG_TYPE_PUBLIC
			);
		return tagSF;
	}




	/********************** Static Methods ***********************/

	public static List<TagCustom> fromSobjects(List<Article__Tag> tagAssignments) {
		List<TagCustom> tags = new List<TagCustom>();
		for (Article__Tag tagAssignment : tagAssignments) {
			TagCustom tag = new TagCustom(tagAssignment);
			tags.add(tag);
		}
		return tags;
	}

	public static List<TagCustom> fromSobjects(List<TagDefinition> tagsSF) {
		List<TagCustom> tags = new List<TagCustom>();
		for (TagDefinition tagSF : tagsSF) {
			TagCustom tag = new TagCustom(tagSF);
			tags.add(tag);
		}
		return tags;
	}

	public static List<TagCustom> getAll() {
		List<TagDefinition> tagsSF = DAL.getAllTags();
		return fromSObjects(tagsSF);
	}

	/*
	 * Updates tags assignments:
	 *   Inserts new and existing tags (tags can be inserted again with no problem)
	 */
	public static void saveTags(List<TagCustom> tags, String articleId) {
		Utils.log('saveTags:' + tags);
		List<Article__Tag> tagsToInsert = new List<Article__Tag>();
		for (TagCustom tag : tags) {
			if (String.isEmpty(tag.assignmentId)) {
				Article__Tag tagSF = tag.toSObject();
				tagSF.ItemId = articleId;
				tagsToInsert.add(tagSF);
			}
		}
		insert tagsToInsert;
	}

	public static void deleteTags(List<String> tagAssignmentsIds) {
		if (tagAssignmentsIds == null) {
			return;
		}
		List<Article__Tag> tagsToDelete = new List<Article__Tag>();
		for (String tagAssignmentId : tagAssignmentsIds) {
			Article__Tag tagToDelete = new Article__Tag(Id = tagAssignmentId);
			tagsToDelete.add(tagToDelete);
		}
		delete tagsToDelete;
	}
}
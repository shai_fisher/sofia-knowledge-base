public with sharing class ArticleCustom {
	private enum DetailMode {FULL_DETAILS, SHORT_DETAILS}

	//private transient Idea ideaObj;
	public String id						{get; private set;}
	public String title						{get; private set;}
	public String categoryId				{get; private set;}
	public String categoryName				{get; private set;}
	public String shortDescription			{get; private set;}
	public String body						{get; private set;}
	public List<TagCustom> tags				{get; private set;}
	public Person publisher					{get; private set;}
	public Datetime postedDate				{get; private set;}
	public List<Comment> comments			{get; private set;}
	public Boolean isEditableByCurrentUser	{get; private set;}
	public List<Attachment> attachments		{get; private set;}
	public Boolean isLikedByCurrentUser		{get; private set;}
	public Integer likes					{get; private set;}
	public Boolean isDraft					{get; private set;}

	// ----------Constructor --------------------
	
	public ArticleCustom(Article__c articleSF) {
		this(articleSF, DetailMode.FULL_DETAILS);
	}

	public ArticleCustom(Article__c articleSF, DetailMode mode) {
		id = articleSF.Id;
		title = articleSF.Title__c;
		categoryId = articleSF.Category__c;
		categoryName = articleSF.Category__r.Name;
		shortDescription = articleSF.Short_Description__c;
		publisher = new Person(articleSF.CreatedBy);
		postedDate = (articleSF.Published_Date__c != null ? articleSF.Published_Date__c : articleSF.CreatedDate);
		isDraft = articleSF.IsDraft__c;
		if (articleSF.Likes_Num__c != null) {
			likes = Integer.valueOf(articleSF.Likes_Num__c);
		}
		
		tags = TagCustom.fromSObjects(articleSF.Tags);
		
		isEditableByCurrentUser = (articleSF.CreatedById == UserInfo.getUserId() || articleSF.OwnerId == UserInfo.getUserId());
		attachments = articleSF.Attachments;
		
		if (mode == DetailMode.FULL_DETAILS) {
			body = articleSF.Body__c;
			comments = fromSobjects(articleSF.Comments__r);
			if (articleSF.Likes_Num__c == null) {
				likes = (String.isNotEmpty(articleSF.Likes__c)) ? articleSF.Likes__c.split(';').size() : 0;
			}
			isLikedByCurrentUser = (articleSF.Likes__c != null && articleSF.Likes__c.containsIgnoreCase(UserInfo.getUserId()));
		}
	}
	
	// ---------- Getters Methods --------------------
	
	public String getId() {
		return id;
	}
	
	// ---------- DB Methods --------------------
	
	public void save() {
		Utils.log('save: ' + this);
		// prepare sObject
		Article__c articleSF = new Article__c(
			Id = id,
			Title__c = title,
			Short_Description__c = shortDescription,
			Body__c = body,
			Category__c = categoryId,
			IsDraft__c = (isDraft == true ? true : false)
		);
		Utils.log('save: articleSF: ' + this);
		// insert sObject
		upsert articleSF;
		id = articleSF.Id;

		// save tag assignments
		TagCustom.saveTags(tags, id);

		// retrieve article with writer and date data
		articleSF = DAL.getArticle(articleSF.Id);
		// put retrieved data in fields
		postedDate = articleSF.CreatedDate;
		publisher = new Person(articleSF.CreatedBy);
	}
	



	/********************** Static Methods ***********************/

	public static List<ArticleCustom> fromSobjects(List<Article__c> articlesSF) {
		List<ArticleCustom> articles = new List<ArticleCustom>();
		for (Article__c articleSF : articlesSF) {
			ArticleCustom article = new ArticleCustom(articleSF, DetailMode.SHORT_DETAILS);
			articles.add(article);
		}
		return articles;
	}

	/*public static List<ArticleCustom> getAllArticles() {
		List<Article__c> articlesSF = DAL.getAllArticles();
		return fromSobjects(articlesSF);
	}*/

	/*public static List<ArticleCustom> getArticles(String category, Set<String> tags) {
		List<Article__c> articlesSF = DAL.getArticles(category, tags);
		return fromSobjects(articlesSF);
	}*/

	public static ArticleCustom getArticle(String ideaId) {
		Article__c articleSF = DAL.getArticle(ideaId);
		return new ArticleCustom(articleSF, DetailMode.FULL_DETAILS);
	}

	public static List<Comment> fromSobjects(List<Comment__c> commentsSF) {
		List<Comment> comments = new List<Comment>();
		for (Comment__c commentSF : commentsSF) {
			Comment comment = new Comment(commentSF);
			comments.add(comment);
		}
		return comments;
	}
	

	
	/********************** Comment inner class ***********************/
	
	public class Comment {
		@TestVisible private String id;
		private String articleId;
		private String body;
		private Person publisher;
		private Datetime postedDate;
		private Boolean isEditableByCurrentUser;

		public Comment(IdeaComment ideaComment) {
			body = ideaComment.CommentBody;
			publisher = new Person(ideaComment.CreatorName);
			postedDate = ideaComment.CreatedDate;
		}
		
		public Comment(Comment__c commentSF) {
			Utils.log('commentSF: ' + commentSF);
			id = commentSF.Id;
			body = commentSF.Body__c;
			publisher = new Person(commentSF.CreatedBy);
			postedDate = commentSF.CreatedDate;
			isEditableByCurrentUser = (commentSF.CreatedById == UserInfo.getUserId());
		}

		public void insertObj() {
			// prepare sObject
			Comment__c commentSF = new Comment__c(
				Id = id, Body__c = body, Article__c = articleId
				);
			Utils.log('insertObj: commentSF: ' + commentSF);
			// insert sObject
			upsert commentSF;
			id = commentSF.id;
			// retrieve comment with writer and date data
			commentSF = DAL.getComment(id);
			// put retrieved data in fields
			postedDate = commentSF.CreatedDate;
			publisher = new Person(commentSF.CreatorName__c);
		}
	}

	/********************** Comment inner class ***********************/

	public class Person {
		private String id;
		private String name;
		private Boolean isEditor;

		public Person(String name) {
			this.name = name;
		}
		
		public Person(User user) {
			if (user != null) {
				this.id = user.Id;
				this.name = user.Name;
			}
		}
	}
}
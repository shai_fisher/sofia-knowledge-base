public with sharing class Utils {

	/******************* log methods ***********************************/
	public static void log(String id, Object str){
		try {
			system.debug('\n\n\n******* ' + id + ': ' + str + ' *******\n\n\n');
		}
		catch(Exception err){
			system.debug('\n\n\n ' + id + ' LOG FUNCTION'+err+'\n\n\n');
		}
	}
	
	public static void log(Object str){
		log('', str);
	}
	
	/********************** Exeptions ***************************/

	public class RemoteException extends Exception {}

	public static void throwRemoteException(Exception e) {
		Utils.log(e);
		throw new RemoteException(e.getMessage() + ',' + e.getStackTraceString(), e);
	}
	
	/*********************8 Other Methods **************************/
	
	public Set<String> getValues(List<SObject> objects, String field) {
		Set<String> values = new Set<String>();
		for (SObject obj : objects) {
			String value = String.valueOf(obj.get(field));
			values.add(value);
		}
		return values;
	}

	public static Attachment addAttachment(string attachmentBody, string parentId, string fileName){
        if(string.isEmpty(attachmentBody))
            return null;
      
        Attachment attachment = new Attachment();
        attachment.Body = EncodingUtil.base64Decode(attachmentBody);
        attachment.Name = String.valueOf(fileName);
        attachment.ParentId = parentId; 
        
        insert attachment;
        return attachment;
    }
}
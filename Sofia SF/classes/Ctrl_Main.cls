public with sharing class Ctrl_Main {

	@RemoteAction
	public static String getArticles(String articlesQueryJson) {
		try {
			Utils.log('getArticles: ' + articlesQueryJson);
			ArticlesQuery articlesQuery = (ArticlesQuery) Json.deserialize(articlesQueryJson, Ctrl_Main.ArticlesQuery.class);
			Utils.log('articlesQuery: ' + articlesQuery);
			List<ArticleCustom> articles = articlesQuery.fetch();
			Utils.log('articles: ' + articles);
			return Json.serialize(articles);
		}
		catch (Exception e) {	Utils.throwRemoteException(e);	return '';	}
	}

	@RemoteAction
	public static String getArticle(String postId) {
		try {
			ArticleCustom postObj = ArticleCustom.getArticle(postId);
			Utils.log('post: ' + postObj);
			return Json.serialize(postObj);
		}
		catch (Exception e) {	Utils.throwRemoteException(e);	return '';	}
	}

	@RemoteAction
	public static String postArticle(String articleJson, List<String> deletedTagsIds) {
		try {
			Utils.log('articleJson: ' + articleJson);
			ArticleCustom articleObj = (ArticleCustom) Json.deserialize(articleJson, ArticleCustom.class);
			articleObj.save();
			TagCustom.deleteTags(deletedTagsIds);
			return articleObj.getId();
		}
		catch (Exception e) {	Utils.throwRemoteException(e);	return '';	}
	}

	@RemoteAction 
    public static void deleteArticle(String articleId){
        utils.log('deleteArticle: ' + articleId);
        Article__c article = new Article__c(Id = articleId);
        delete article;
    }
	
	@RemoteAction
	public static String postComment(String commentJson) {
		try {
			ArticleCustom.Comment comment = (ArticleCustom.Comment) Json.deserialize(commentJson, ArticleCustom.Comment.class);
			comment.insertObj();
			commentJson = Json.serialize(comment);
			return commentJson;
		}
		catch (Exception e) {	Utils.throwRemoteException(e);	return '';	}
	}

	@RemoteAction
	public static void deleteComment(String commentId) {
		Comment__c comment = new Comment__c(Id = commentId);
		delete comment;
	}

	@RemoteAction
	public static void likeArticle(String articleId) {
		try {
			Article__c articleSF = DAL.getArticleLikes(articleId);
			String userId = UserInfo.getUserId();
			if (articleSF.Likes__c == null) {
				articleSF.Likes__c = '';
			}
			if (!articleSF.Likes__c.containsIgnoreCase(userId)) {
				articleSF.Likes__c += userId + ';';
				update articleSF;
			}
		}
		catch (Exception e) {	Utils.throwRemoteException(e);	}
	}

	@RemoteAction
	public static void dislikeArticle(String articleId) {
		try {
			Article__c articleSF = DAL.getArticleLikes(articleId);
			String userId = UserInfo.getUserId();
			if (articleSF.Likes__c != null || articleSF.Likes__c.containsIgnoreCase(userId)) {
				articleSF.Likes__c = articleSF.Likes__c.replaceAll(userId + ';', '');
				articleSF.Likes__c = articleSF.Likes__c.replaceAll(userId, '');
				update articleSF;
			}
		}
		catch (Exception e) {	Utils.throwRemoteException(e);	}
	}

	@RemoteAction
	public static String getAllTags() {
		try {
			List<TagCustom> tags = TagCustom.getAll();
			return Json.serialize(tags);
		}
		catch (Exception e) {	Utils.throwRemoteException(e);	return '';	}
	}

	@RemoteAction
	public static String getAllCatgories() {
		try {
			List<Category__c> categories = DAL.getAllCategories();
			return Json.serialize(categories);
		}
		catch (Exception e) {	Utils.throwRemoteException(e);	return '';	}
	}

	@RemoteAction 
    public static String saveAttachment(String parentId, String base64str, String fileName){
        utils.log('saveAttachment: ' + parentId);
        Attachment newAttachment = Utils.addAttachment(base64str, parentId, fileName);
        return newAttachment.Id;
    }

    @RemoteAction 
    public static void deleteAttachment(String attachmentId){
        utils.log('deleteAttachment: ' + attachmentId);
        Attachment attachment = new Attachment(Id = attachmentId);
        delete attachment;
    }

	@RemoteAction
	public static String getTitlePages() {
		List<Title_Page__c> titlePages = DAL.getTitlePages();
		return Json.serialize(titlePages);
	}
	
    @RemoteAction
	public static String getTitlePage(String titlePageId) {
		TitlePageCustom titlePage = TitlePageCustom.getTitlePage(titlePageId);
		return Json.serialize(titlePage);
	}
	
	@RemoteAction
	public static String saveTitlePage(String titlePageJson) {
		Utils.log('saveTitlePage: ' + titlePageJson);
		TitlePageCustom titlePage = (TitlePageCustom) Json.deserialize(titlePageJson, TitlePageCustom.class);
		titlePage.save();
		return titlePage.id;
	}

	@RemoteAction
	public static Void deleteTitlePage(Id titlePageId) {
		Database.delete(titlePageId);
	}


    /************************ ArticlesQuery inner class *******************/

    @TestVisible
    private class ArticlesQuery {
    	private boolean myArticles;
    	private boolean drafts;
    	private String category;
    	private List<String> tags;
    	private String search;

    	public List<ArticleCustom> fetch() {
    		Utils.log('fetch: myArticles:' + myArticles + ', category: ' + category + ', tags: ' + tags + ', search: ' + search);
    		Set<String> tagsSet = (tags != null ? new Set<String>(tags) : new Set<String>());
    		List<Article__c> articlesSF = DAL.getArticles(myArticles, drafts, category, tagsSet, search);
    		return ArticleCustom.fromSobjects(articlesSF);
    	}
    }
}
@isTest
private class Test_Ctrl_Main {
	
	private static testMethod void test_CreateArticle() {
		String articleJson = '{"body":"<p>Body</p>","title":"Title","shortDescription":"Short Description","categoryId":"a05b000000H9Eg1AAF","tags":[{"name":"tag1"}]}';
		String articleId = Ctrl_Main.postArticle(articleJson, null);
		System.assert(String.isNotEmpty(articleId));
	}
	
	private static testMethod void test_likeArticle() {
		ArticleCustom articleObj = Test_Factory.createArticle();
		System.assertEquals(0, articleObj.likes);

		Ctrl_Main.likeArticle(articleObj.id);
		articleObj = ArticleCustom.getArticle(articleObj.id);
		System.assertEquals(1, articleObj.likes);
		System.assertEquals(true, articleObj.isLikedByCurrentUser);

		Ctrl_Main.dislikeArticle(articleObj.id);
		articleObj = ArticleCustom.getArticle(articleObj.id);
		System.assertEquals(0, articleObj.likes);
	}

	private static testMethod void test_getArticles() {
		Category__c category = Test_Factory.createCategory();
		Article__c articleSF = Test_Factory.createArticleSF(category);
		Test_Factory.addTag(articleSF, 'tag1');

		Utils.log('Real results: ' + [select Id, IsDraft__c, Category__r.Name from Article__c]);

		Test.startTest();
			String articlesQueryJson = '{"category": "' + category.Name + '", "tags": ["tag1"]}';
			String articlesJson = Ctrl_Main.getArticles(articlesQueryJson);
			List<ArticleCustom> articles = (List<ArticleCustom>) Json.deserialize(articlesJson, List<ArticleCustom>.class);
			System.assertEquals(1, articles.size());
			System.assertEquals(category.Name, articles[0].categoryName);
			System.assertNotEquals(null, articles[0].tags);
			System.assertEquals(1, articles[0].tags.size());
			System.assertEquals('tag1', articles[0].tags[0].name);
		Test.stopTest();
	}

	private static testMethod void test_searchArticles() {
		Article__c articleSF = Test_Factory.createArticleSF();
		articleSF.Body__c = 'ganan gidel dagan bagan';
		update articleSF;
		// set SOSL result for test
		Test.setFixedSearchResults(new List<Id> { articleSF.Id });

		Test.startTest();
			String articlesQueryJson = '{"search": "gidel"}';
			String articlesJson = Ctrl_Main.getArticles(articlesQueryJson);
			List<ArticleCustom> articles = (List<ArticleCustom>) Json.deserialize(articlesJson, List<ArticleCustom>.class);
			System.assertEquals(1, articles.size());
		Test.stopTest();
	}

	private static testMethod void test_getArticle() {
		Article__c articleSF = Test_Factory.createArticleSF();

		Test.startTest();
			String articleJson = Ctrl_Main.getArticle(articleSF.Id);
			ArticleCustom articleObj = (ArticleCustom) Json.deserialize(articleJson, ArticleCustom.class);
			System.assertEquals(articleSF.Id, articleObj.id);
		Test.stopTest();
	}

	private static testMethod void test_deleteArticle() {
		Article__c articleSF = Test_Factory.createArticleSF();

		Test.startTest();
			Ctrl_Main.deleteArticle(articleSF.Id);
			List<Article__c> articles = [select Id from Article__c where Id = :articleSF.Id];
			System.assertEquals(0, articles.size());
		Test.stopTest();
	}

	private static testMethod void test_postComment() {
		Article__c articleSF = Test_Factory.createArticleSF();

		Test.startTest();
			String commentJson = '{"articleId": "' + articleSF.Id + '"}';
			commentJson = Ctrl_Main.postComment(commentJson);
			ArticleCustom.Comment comment = (ArticleCustom.Comment) JSON.deserialize(commentJson, ArticleCustom.Comment.class);
			List<Comment__c> comments = [select Id from Comment__c where Id = :comment.Id];
			System.assertEquals(1, comments.size());
		Test.stopTest();
	}

	private static testMethod void test_getAllTags() {
		Article__c articleSF = Test_Factory.createArticleSF();

		Test.startTest();
			string tagsJson = Ctrl_Main.getAllTags();
			List<TagCustom> tags = (List<TagCustom>) JSON.deserialize(tagsJson, List<TagCustom>.class);
			// creating article__Tag somehow dowsnt create TagDefinition in test. maybe takes time
			//System.assertEquals(1, tags.size());
			//System.assertEquals('test_tag', tags[0].Name);
		Test.stopTest();
	}

	private static testMethod void test_getAllCatgories() {
		Category__c category = Test_Factory.createCategory();

		Test.startTest();
			string catgoriesJson = Ctrl_Main.getAllCatgories();
			List<Category__c> catgories = (List<Category__c>) JSON.deserialize(catgoriesJson, List<Category__c>.class);
			System.assertEquals(1, catgories.size());
			System.assertEquals(category.Name, catgories[0].Name);
		Test.stopTest();
	}

	private static testMethod void test_saveAttachment() {
		Article__c articleSF = Test_Factory.createArticleSF();
		Test.startTest();
			String attachmentId = Ctrl_Main.saveAttachment(articleSF.Id, 'test64', 'filename');
			List<Attachment> attachments = [select Id, Name from Attachment where Id = :attachmentId];
			System.assertEquals(1, attachments.size());
		Test.stopTest();
	}

	private static testMethod void test_deleteAttachment() {
		Article__c articleSF = Test_Factory.createArticleSF();
		Attachment attachment = Test_Factory.createAttachment(articleSF.Id);
		Test.startTest();
			Ctrl_Main.deleteAttachment(attachment.Id);
			List<Attachment> attachments = [select Id from Attachment where Id = :attachment.Id];
			System.assert(attachments.isEmpty());
		Test.stopTest();
	}

	private static testMethod void test_getTitlePages() {
		Title_Page__c titlePage = Test_Factory.createTitlePage('test');
		Title_Page_Item__c titlePageItem = Test_Factory.createTitlePageItem(titlePage);
		Test.startTest();
			String titlePagesJson = Ctrl_Main.getTitlePages();
			List<Title_Page__c> titlePages = (List<Title_Page__c>) JSON.deserialize(titlePagesJson, List<Title_Page__c>.class);
			System.assertEquals(1, titlePages.size());
		Test.stopTest();
	}

	private static testMethod void test_getTitlePage() {
		Title_Page__c titlePageSF = Test_Factory.createTitlePage('test');
		Title_Page_Item__c titlePageItem = Test_Factory.createTitlePageItem(titlePageSF);
		Test.startTest();
			String titlePageJson = Ctrl_Main.getTitlePage(titlePageSF.Id);
			TitlePageCustom titlePage = (TitlePageCustom) JSON.deserialize(titlePageJson, TitlePageCustom.class);
			System.assertEquals(titlePageSF.Id, titlePage.id);
			System.assertEquals(titlePageSF.Name, titlePage.name);
			System.assertEquals(1, titlePage.items.size());
		Test.stopTest();
	}

	private static testMethod void test_saveTitlePage() {
		Test.startTest();
		Test.stopTest();
	}

	private static testMethod void test_deleteTitlePage() {
		Test.startTest();
		Test.stopTest();
	}

}
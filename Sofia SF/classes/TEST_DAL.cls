@isTest
private class TEST_DAL {

	private static testMethod void test_getAllTags() {
		Article__c articleSF = Test_Factory.createArticleSF();
		Test_Factory.addTag(articleSF, 'test_tag');

		Test.startTest();
			List<TagDefinition> tags = DAL.getAllTags();
			// creating article__Tag somehow dowsnt create TagDefinition in test. maybe takes time
			//System.assertEquals(1, tags.size());
			//System.assertEquals('test_tag', tags[0].Name);
		Test.stopTest();
	}
}

public with sharing class TitlePageCustom {
	
	public Id id							{get; set;}
	public String name						{get; set;}
	public List<Item> items					{get; set;}
	public List<Item> deletedItems			{get; set;}
	public Boolean isEditableByCurrentUser	{get; set;}


	public TitlePageCustom(Title_Page__c sfTitlePage) {
		id = sfTitlePage.Id;
		name = sfTitlePage.Name;
		items = convertItems(sfTitlePage.Title_Page_Items__r);
		deletedItems = new List<Item>();
		isEditableByCurrentUser = (sfTitlePage.CreatedById == UserInfo.getUserId() || sfTitlePage.OwnerId == UserInfo.getUserId());
	}

	public void save() {
		Boolean isNew = String.isEmpty(id);
		Title_Page__c sfTitlePage = new Title_Page__c(
			Id = id,
			Name = name
		);
		upsert sfTitlePage;
		id = sfTitlePage.Id;

		if (items != null && !items.isEmpty()) {
			if (isNew) {
				for (Item item : items) {
					item.titlePageId = id;
				}
			}
			List<Title_Page_Item__c> sfItems = convertItems(items);
			upsert sfItems;
		}
		if (deletedItems != null && !deletedItems.isEmpty()) {
			List<Title_Page_Item__c> sfItems = convertItems(deletedItems);
			delete sfItems;
			deletedItems.clear();
		}
	}

	//---------------------------- static methods -------------------------------------

	public static TitlePageCustom getTitlePage(String titlePageId) {
		Title_Page__c sfTitlePage = DAL.getTitlePage(titlePageId);
		return new TitlePageCustom(sfTitlePage);
	}

	public static List<Item> convertItems(List<Title_Page_Item__c> sfItems) {
		List<Item> items = new List<Item>();
		for (Title_Page_Item__c sfItem : sfItems) {
			items.add(new Item(sfItem));
		}
		return items;
	}

	public static List<Title_Page_Item__c> convertItems(List<Item> items) {
		List<Title_Page_Item__c> sfItems = new List<Title_Page_Item__c>();
		for (Item item : items) {
			sfItems.add(item.toSObject());
		}
		return sfItems;
	}


	//---------------------------- Item inner class -------------------------------------

	private class Item {
		public String id				{get; set;}
		public String titlePageId		{get; set;}
		public String articleId			{get; set;}
		public Integer row				{get; set;}
		public Integer col				{get; set;}
		public Integer sizeX			{get; set;}
		public Integer sizeY			{get; set;}
		public ArticleCustom article	{get; set;}

		public Item(Title_Page_Item__c sfItem) {
			id = sfItem.Id;
			articleId = sfItem.Article__c;
			row = (Integer) sfItem.Row__c;
			col = (Integer) sfItem.Col__c;
			sizeX = (Integer) sfItem.Colspan__c;
			sizeY = (Integer) sfItem.Rowspan__c;
			article = new ArticleCustom(sfItem.Article__r);
		}

		public Title_Page_Item__c toSObject() {
			return new Title_Page_Item__c(
				Id = id,
				Title_Page__c = titlePageId,
				Article__c = articleId,
				Row__c = row,
				Col__c = col,
				Rowspan__c = sizeY,
				Colspan__c = sizeX
			);
		}
	}

}